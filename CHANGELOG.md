# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
Quick remainder of the possible sections:
-----------------------------------------
### Added
  for new features.
### Changed
  for changes in existing functionality.
### Deprecated
  for soon-to-be removed features.
### Removed
  for now removed features.
### Fixed
  for any bug fixes.
### Security
  in case of vulnerabilities.
### Maintenance
  in case of rework, dependencies change

Please, keep them in this order when updating.
-->

## [Unreleased]

## [0.2.2] - 2024-11-01

### Removed

- Removed date from generated report 

## [0.2.1] - 2024-10-02

### Fixed

- Fixed spec status value based on the last run. It now uses the correct current status.

## [0.2.0] - 2023-11-10 - JSON report and fixes

### Added

- Support for beautified JSON reports
- Links to details in summary table

### Fixed

- Don't display example type when empty

## [0.1.1] - 2023-11-10 - Fixes

### FIXED
- Fix missing require in Specs class file
- Fix invalid require in usage example README

## [0.1.0] - 2023-11-07 - Initial release

Initial release

