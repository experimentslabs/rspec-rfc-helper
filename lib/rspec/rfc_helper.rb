# frozen_string_literal: true

require_relative 'rfc_helper/version'
require_relative 'rfc_helper/specs'

module RSpec
  ##
  # RSpec module to track specifications with more context
  module RfcHelper
    class Error < StandardError; end

    ##
    # Delegation when using the module as a starting point
    def self.add_example(example)
      raise 'Did you start?' unless @specs

      @specs.add_example(example)
    end

    ##
    # Delegation when using the module as a starting point
    def self.start_from_file(file)
      raise 'Already started' if @specs

      @specs = Specs.new_from_file file
    end

    def self.save_markdown_report(file)
      raise 'Did you start?' unless @specs

      @specs.save_markdown_report file
    end

    def self.save_json_report(file)
      raise 'Did you start?' unless @specs

      @specs.save_json_report file
    end
  end
end
