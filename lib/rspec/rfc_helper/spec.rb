# frozen_string_literal: true

module RSpec
  module RfcHelper
    ##
    # Represents one spec
    class Spec
      attr_reader :id, :section, :verb, :text, :examples

      ##
      # Initializes a new Spec
      #
      # @param section [String]           Section number
      # @param text    [String]           Paragraph from the RFC.
      # @param id      [Symbol, NilClass] Unique identifier
      #
      # +text+ should have one _verb_ surrounded by brackets, representing what this Spec expects:
      #
      #   bad: It [[MUST]] work with a missing user but [[MAY]] log a warning
      #
      # Use two Specs instead:
      #   It [[MUST]] work with a missing user but MAY log a warning
      #   It MUST work with a missing user but [[MAY]] log a warning
      #
      # When nothing is highlighted, spec will appear as a "note".
      def initialize(section:, text:, id: nil)
        raise "Missing text ({section: #{section}, text: #{text}, id: #{id})" unless text.is_a?(String) && !text.empty?
        raise "Missing section ({section: #{section}, text: #{text}, id: #{id})" unless section.is_a?(String) && !section.empty?

        @id       = id
        @section  = section.to_s
        @text     = text
        @examples = []
        set_verb
      end

      ##
      # Determines status from RSpec examples
      #
      # @return [:unknown|:pass|:has_pending|:failing] Spec state from all examples
      #
      # Statuses:
      #   unknown: No test currently covers this spec
      #   failing: At least one example failed
      #   has_pending: Some examples are pending, other passed
      #   pass: All examples passed
      def status
        value = :unknown
        @examples.each do |example|
          return :failing if example.execution_result.status == :failed
          return :has_pending if example.execution_result.status == :pending
          raise "Unhandled example status: #{example.execution_result.status}" if example.execution_result.status != :passed

          value = :pass
        end

        value
      end

      ##
      # Adds an RSpec example
      #
      # @param example [RSpec::Core::Example] RSpec example
      def add_example(example)
        @examples << example
      end

      private

      ##
      # Determines the verb of interest in the given text; falls back to +:note+
      #
      # The key words MAY, MUST, MUST NOT, SHOULD, and SHOULD NOT are to be interpreted as described in RFC2119:
      # @see https://www.w3.org/TR/activitypub/#bib-RFC2119
      def set_verb
        occurrences = @text.scan(/\[\[((?:MUST|SHOULD)(?: NOT)?|MAY)\]\]/)
        case occurrences.size
        when 0
          @verb = :note
        when 1
          @verb = occurrences.first.first.downcase.tr(' ', '_').to_sym
        else
          raise 'Too much verbs in paragraph' if occurrences.size > 1
        end
      end
    end
  end
end
