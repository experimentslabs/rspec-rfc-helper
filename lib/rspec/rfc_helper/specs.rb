# frozen_string_literal: true

require 'yaml'

require_relative 'spec'
require_relative 'markdown_renderer'
require_relative 'json_renderer'

module RSpec
  module RfcHelper
    ##
    # Spec collection
    class Specs
      attr_reader :name, :specs_url, :sections, :specs

      ##
      # Instantiates a Spec collection from a YAML file.
      #
      # Note: String keys are expected.
      #
      # @param file [String] Path to the specs file
      #
      # @return [RSpec::RfcHelper::Specs]
      def self.new_from_file(file) # rubocop:disable Metrics/AbcSize
        specs = YAML.load_file(file)

        instance = new name: specs['name'], specs_url: specs['url']

        specs['specs'].each do |section|
          section_number = section['section'].to_s

          instance.add_section id: section['id'].to_sym, number: section_number, title: section['title'], url: section['url']

          section['specs'].each do |spec|
            instance.add section: section_number, text: spec['text'], id: spec['id']&.to_sym
          end
        end

        instance
      end

      ##
      # Instantiate a new list of specs
      #
      # @param name      [String?] Specification name
      # @param specs_url [String?] URL to the full specification
      def initialize(name: nil, specs_url: nil)
        @name      = name
        @specs_url = specs_url
        @sections  = {}
        @specs     = []
      end

      ##
      # Adds a spec section
      #
      # @param number [String]  Section number
      # @param title  [String]  Section title
      # @param id     [Symbol]  Unique identifier
      # @param url    [String?] URL to this section
      def add_section(number:, title:, id:, url: nil)
        if @sections.key?(number) || @sections.find { |_number, section| section[:id] == id }
          raise "Section number #{number} already exists"
        end

        @sections[number] = { title: title, id: id, url: url }
      end

      ##
      # Finds an example by id
      #
      # @param id [Symbol] Spec identifier
      #
      # @return [RSpec::RfcHelper::Spec]
      def find(id)
        @specs.find { |spec| spec.id == id } || raise("Spec not found with id #{id}")
      end

      ##
      # Adds a spec to the list
      #
      # It will prefix the spec ID with relevant section ID
      #
      # @param section [String]  Section number
      # @param text    [String]  Content
      # @param id      [Symbol?] Unique section identifier
      def add(section:, text:, id: nil)
        if id.is_a? Symbol
          id = "#{section_id(section)}__#{id}".to_sym
          raise "Duplicated id #{id}" if ids.include? id
        end

        @specs << Spec.new(section: section, text: text, id: id)
      end

      ##
      # Adds an RSpec example to all the relevant specs
      #
      # @param example [RSpec::Core::Example]
      def add_example(example)
        ids = example.metadata[:rfc]
        return if ids.nil? || ids.empty?

        ids = [ids] if ids.is_a? Symbol
        ids.each do |id|
          find(id).add_example example
        end
      end

      def ids
        @specs.map(&:id)
      end

      ##
      # Delegation
      def each(&)
        @specs.each(&)
      end

      ##
      # Saves the report in Markdown
      #
      # @param file [String] Output file path
      def save_markdown_report(file)
        File.write file, RSpec::RfcHelper::MarkdownRenderer.new(self).render
      end

      ##
      # Saves the report in JSON
      #
      # @param file [String] Output file path
      def save_json_report(file)
        File.write file, RSpec::RfcHelper::JsonRenderer.new(self).render
      end

      private

      ##
      # @param number [String] Section number
      #
      # @return [Symbol] Section ID from its number
      def section_id(number)
        section = @sections[number] || raise("Section not found with number #{number}")
        section[:id]
      end
    end
  end
end
