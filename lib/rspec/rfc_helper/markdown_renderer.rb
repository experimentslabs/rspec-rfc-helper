# frozen_string_literal: true

module RSpec
  module RfcHelper
    ##
    # Markdown renderer for specs
    class MarkdownRenderer
      STATUS_COLORS = {
        # Status from Spec class
        failing:     'red',
        pass:        'green',
        has_pending: 'orange',
        unknown:     'gray',

        # Statuses from RSpec::Core::Example
        failed:      'red',
        pending:     'orange',
        passed:      'green',
      }.freeze

      ##
      # @param specs [RSpec::RfcHelper::Specs]
      def initialize(specs)
        @specs = specs
      end

      ##
      # Generates a report in Markdown format.
      #
      # @return [String]
      def render
        main_table_lines = []
        detailed_spec_report_blocks = []

        @specs.each do |spec|
          main_table_lines << main_table_row(spec)
          detailed_spec_report_blocks << detailed_spec_report_block(spec)
        end

        <<~MD.chomp
          #{header}

          #{main_table(main_table_lines)}

          #{detailed_spec_report_blocks.join("\n")}
        MD
      end

      ##
      # Generates the report header
      #
      # @return [String]
      def header
        title = 'Compliance status'
        title = "#{title}: #{@specs.name}" if @specs.name
        url = @specs.specs_url ? "[#{@specs.specs_url}](#{@specs.specs_url})" : '~'

        <<~MD.chomp
          # #{title}

          Source specification: #{url}
        MD
      end

      ##
      # Generates a colored HTML string to represent the spec status
      #
      # @param value  [String, Symbol] Status
      # @param string [String?]        Custom text replacement
      #
      # @return [String]
      def status(value, string = nil)
        "<span style='color: #{STATUS_COLORS[value]}'>#{string || value}</span>"
      end

      ##
      # Makes some replacement in a text
      #
      # @param string [String]
      #
      # @return [String]
      def paragraph(string)
        string.gsub("\n", ' <br> ')
              .gsub(/\[\[([^\]]*)\]\]/, '<mark>\1</mark>')
      end

      ##
      # Generates a spec report block (header and examples table)
      #
      # @param spec [RSpec::RfcHelper::Spec]
      #
      # @return [String]
      def detailed_spec_report_block(spec)
        <<~MD.chomp
          #{detailed_spec_report_header(spec)}

          #{detailed_spec_report_examples(spec)}
        MD
      end

      ##
      # Generates header for a spec report
      #
      # @param spec [RSpec::RfcHelper::Spec]
      #
      # @return [String]
      def detailed_spec_report_header(spec) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
        section = "#{spec.section} - #{@specs.sections[spec.section][:title]}"
        spec_link = if @specs.sections[spec.section][:url]
                      "[#{section}](#{@specs.sections[spec.section][:url]})"
                    else
                      section
                    end
        anchor = spec.id ? "<a id=\"#{spec.id}\"></a>" : ''
        <<~MD.chomp
          ## #{anchor}#{status(spec.status, '○')} `#{spec.verb.upcase}` #{spec.id}

          **Specification:** #{spec_link}

          > #{paragraph(spec.text)}
        MD
      end

      ##
      # Generates the example table of a spec report
      #
      # @param spec [RSpec::RfcHelper::Spec]
      #
      # @return [String]
      def detailed_spec_report_examples(spec) # rubocop:disable Metrics/MethodLength
        lines = []
        spec.examples.each do |example|
          meta = example.metadata
          type = meta[:type] ? "`#{meta[:type]}`: " : ''
          lines << "| #{status(example.execution_result.status)} | #{type}`#{meta[:location]}` |"
        end

        return "**No related examples**\n" if lines.empty?

        <<~MD.chomp
          **Examples:**

          | Status | Reference |
          |:------:|-----------|
          #{lines.join("\n")}

        MD
      end

      ##
      # Generates a row for the main table
      #
      # @param spec [RSpec::RfcHelper::Spec]
      #
      # @return [String]
      def main_table_row(spec)
        section = @specs.sections[spec.section]
        section_link = section[:url] ? "[#{spec.section}](#{section[:url]})" : spec.section
        id_link = spec.id ? "[`#{spec.id}`](##{spec.id})" : ''
        "| #{status(spec.status)} | #{id_link} | #{section_link} | #{spec.verb} | #{paragraph(spec.text)} |"
      end

      ##
      # Generates the main table
      #
      # @param lines [Array<String>] Table rows
      #
      # @return [String]
      def main_table(lines)
        <<~MD.chomp
          | status | id | section | verb | text |
          |-------:|---:|---------|------|------|
          #{lines.join("\n")}
        MD
      end
    end
  end
end
