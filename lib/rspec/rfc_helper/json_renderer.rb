# frozen_string_literal: true

module RSpec
  module RfcHelper
    ##
    # Markdown renderer for specs
    class JsonRenderer
      ##
      # @param specs [RSpec::RfcHelper::Specs]
      def initialize(specs)
        @specs = specs
      end

      ##
      # Generates a report in JSON format.
      #
      # @return [String]
      def render
        json = {
          name:     @specs.name,
          url:      @specs.specs_url,
          specs:    specs,
          sections: sections,
        }

        JSON.pretty_generate json
      end

      private

      ##
      # Extracts sections informations
      #
      # @return [Array<Hash>]
      def sections
        list = []

        @specs.sections.each_pair do |number, section|
          list.push id:     section[:id],
                    title:  section[:title],
                    number: number,
                    url:    section[:url]
        end

        list
      end

      ##
      # Extracts specs and examples
      #
      # @return [Array<Hash>]
      def specs
        list = []
        @specs.specs.each do |spec|
          list.push id:       spec.id,
                    section:  spec.section,
                    verb:     spec.verb,
                    text:     spec.text,
                    examples: examples(spec)
        end

        list
      end

      ##
      # Extracts examples statuses from a spec
      #
      # @param spec [RSpec::RfcHelper::Spec]
      #
      # @return [Array<Hash>]
      def examples(spec)
        list = []
        spec.examples.each do |example|
          meta = example.metadata
          list.push status:   example.execution_result.status,
                    type:     meta[:type],
                    location: meta[:location]
        end

        list
      end
    end
  end
end
