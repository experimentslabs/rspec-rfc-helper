# frozen_string_literal: true

class FakeExecutionResult
  # @see RSpec::Core::Example::ExecutionResult @status
  STATUSES = [:passed, :failed, :pending].freeze

  attr_reader :status

  def initialize(status)
    raise "Unsupported status #{status} (valid statuses are #{STATUSES.join(', ')}" unless STATUSES.include? status

    @status = status
  end
end

class FakeRSpecExample
  attr_reader :metadata, :execution_result

  # @param status [:passed|:failed|:pending] Example status
  def initialize(status, metadata = {})
    @status = status
    @metadata = metadata
    @execution_result = FakeExecutionResult.new status
  end
end
