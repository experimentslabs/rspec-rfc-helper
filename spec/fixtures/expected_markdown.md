# Compliance status: The Ultimate Specification

Source specification: [https://example.com](https://example.com)

| status | id | section | verb | text |
|-------:|---:|---------|------|------|
| <span style='color: green'>pass</span> | [`first__be_good`](#first__be_good) | [1](https://example.com/#section1) | must | It <mark>MUST</mark> be good |
| <span style='color: green'>pass</span> | [`sub__work`](#sub__work) | [1.1](https://example.com/#section2) | should | It <mark>SHOULD</mark> work, but it is not necessary |
| <span style='color: red'>failing</span> | [`sub__make_test_better`](#sub__make_test_better) | [1.1](https://example.com/#section2) | may | It <mark>MAY</mark> make tests better |
| <span style='color: gray'>unknown</span> | [`second__non_implementation`](#second__non_implementation) | [2](https://example.com/#section2) | should_not | Something <mark>SHOULD NOT</mark> be implemented |
| <span style='color: gray'>unknown</span> | [`second__colored_output`](#second__colored_output) | [2](https://example.com/#section2) | should_not | It <mark>SHOULD NOT</mark> use grayscale output |
| <span style='color: gray'>unknown</span> |  | 3 | note | Some thing to implement |

## <a id="first__be_good"></a><span style='color: green'>○</span> `MUST` first__be_good

**Specification:** [1 - The first section](https://example.com/#section1)

> It <mark>MUST</mark> be good

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: green'>passed</span> | `system`: `some_spec.rb:20` |

## <a id="sub__work"></a><span style='color: green'>○</span> `SHOULD` sub__work

**Specification:** [1.1 - A subsection](https://example.com/#section2)

> It <mark>SHOULD</mark> work, but it is not necessary

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: green'>passed</span> | `system`: `some_spec.rb:20` |

## <a id="sub__make_test_better"></a><span style='color: red'>○</span> `MAY` sub__make_test_better

**Specification:** [1.1 - A subsection](https://example.com/#section2)

> It <mark>MAY</mark> make tests better

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: red'>failed</span> | `routing`: `some_spec2.rb:49` |
| <span style='color: orange'>pending</span> | `routing`: `some_spec2.rb:1` |

## <a id="second__non_implementation"></a><span style='color: gray'>○</span> `SHOULD_NOT` second__non_implementation

**Specification:** [2 - The second section](https://example.com/#section2)

> Something <mark>SHOULD NOT</mark> be implemented

**No related examples**

## <a id="second__colored_output"></a><span style='color: gray'>○</span> `SHOULD_NOT` second__colored_output

**Specification:** [2 - The second section](https://example.com/#section2)

> It <mark>SHOULD NOT</mark> use grayscale output

**No related examples**

## <span style='color: gray'>○</span> `NOTE` 

**Specification:** 3 - Where things are missing

> Some thing to implement

**No related examples**
