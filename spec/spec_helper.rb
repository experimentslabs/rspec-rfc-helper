# frozen_string_literal: true

require 'simplecov'

SimpleCov.start

require 'rspec/rfc_helper'

# Support files
require 'support/fake_rspec_example'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  rfc_helper = RSpec::RfcHelper::Specs.new_from_file 'spec/rfc.yaml'

  config.after do |example|
    rfc_helper.add_example example
  end

  config.after(:suite) do
    rfc_helper.save_markdown_report 'report.md'
  end
end
