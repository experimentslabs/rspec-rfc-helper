# frozen_string_literal: true

require 'spec_helper'
require 'rspec/rfc_helper/specs'

RSpec.describe RSpec::RfcHelper::Specs do
  let(:instance) { described_class.new }

  describe '#add_section' do
    it 'adds a section' do
      expect do
        instance.add_section number: '1', title: 'The section 1', id: :first
      end.to change { instance.instance_variable_get(:@sections).keys.count }.by 1
    end

    context 'when the section already exists' do
      before do
        instance.add_section number: '1', title: 'The section 1', id: :first
      end

      it 'raises an exception when number exists' do
        expect do
          instance.add_section number: '1', title: 'Duplicated number', id: :something
        end.to raise_error(/^Section .* already exists/)
      end

      it 'raises an exception when id exists' do
        expect do
          instance.add_section number: '2', title: 'Duplicated id', id: :first
        end.to raise_error(/^Section .* already exists/)
      end
    end
  end

  describe '#find' do
    before do
      instance.add_section number: '1', title: 'First section', id: :first
      instance.add section: '1', text: 'It [[SHOULD]] pass', id: :some_id
    end

    it 'returns the right spec' do
      spec = instance.find(:first__some_id)

      aggregate_failures do
        expect(spec).to be_a RSpec::RfcHelper::Spec
        expect(spec.id).to eq :first__some_id
      end
    end

    context 'when the spec does not exist' do
      it 'raises an exception' do
        expect do
          instance.find :inexistant
        end.to raise_error(/^Spec not found/)
      end
    end
  end

  describe '#add', rfc: :features__can_define_spec do
    before do
      instance.add_section number: '1', title: 'First section', id: :first
    end

    it 'prefixes the spec ID' do
      instance.add section: '1', text: 'It [[SHOULD]] pass', id: :my_id
      added = instance.instance_variable_get(:@specs).last
      expect(added.id).to eq :first__my_id
    end

    context 'when a spec already exists with the same id' do
      before do
        instance.add section: '1', text: 'It [[SHOULD]] pass', id: :my_id
      end

      it 'raises an exception', rfc: :features__identify_specs_with_id do
        expect do
          instance.add section: '1', text: 'Something completely different', id: :my_id
        end.to raise_error(/^Duplicated id/)
      end
    end
  end

  describe '#add_example' do
    before do
      instance.add_section number: '1', title: 'First section', id: :section_one
      instance.add section: '1', text: 'It [[SHOULD]] pass', id: :spec1
      instance.add section: '1', text: 'It [[SHOULD]] pass', id: :spec2
      instance.add section: '1', text: 'It [[SHOULD]] pass', id: :spec3
    end

    it 'adds the example to all the relevant specs', rfc: [:reporting__rfc_tag, :reporting__rfc_with_single_id_or_list] do # rubocop:disable RSpec/ExampleLength
      instance.add_example FakeRSpecExample.new(:passed, { rfc: [:section_one__spec1, :section_one__spec2] })

      aggregate_failures do
        expect(instance.find(:section_one__spec1).examples.count).to eq 1
        expect(instance.find(:section_one__spec2).examples.count).to eq 1
        expect(instance.find(:section_one__spec3).examples.count).to eq 0
      end
    end
  end

  describe '#save_markdown_report' do
    it 'writes the file', rfc: :reporting__export_method do
      allow(File).to receive(:write)

      instance.save_markdown_report('foo')

      expect(File).to have_received(:write).once
    end
  end

  describe '.new_from_file' do
    let(:instance) { described_class.new_from_file 'spec/fixtures/rfc.yaml' }

    it 'has the right sections' do # rubocop:disable RSpec/ExampleLength
      sections = instance.instance_variable_get(:@sections)
      first = sections['1']
      aggregate_failures do
        expect(sections.keys.count).to eq 4
        expect(first[:title]).to eq 'The first section'
        expect(first[:url]).to eq 'https://example.com/#section1'
        expect(first[:id]).to eq :first
      end
    end

    it 'has the right specs' do # rubocop:disable RSpec/ExampleLength
      specs = instance.instance_variable_get(:@specs)
      first = specs.first
      aggregate_failures do
        expect(specs.count).to eq 6
        expect(first.text).to eq 'It [[MUST]] be good'
        expect(first.id).to eq :first__be_good
      end
    end
  end
end
