# frozen_string_literal: true

require 'spec_helper'

require 'rspec/rfc_helper/markdown_renderer'

RSpec.describe RSpec::RfcHelper::MarkdownRenderer do
  let(:spec) { RSpec::RfcHelper::Specs.new_from_file 'spec/fixtures/rfc.yaml' }
  let(:instance) { described_class.new spec }

  before do
    spec.add_example FakeRSpecExample.new :passed, type:     :system,
                                                   location: 'some_spec.rb:20',
                                                   rfc:      [:first__be_good, :sub__work]
    spec.add_example FakeRSpecExample.new :failed, type:     :routing,
                                                   location: 'some_spec2.rb:49',
                                                   rfc:      [:sub__make_test_better]
    spec.add_example FakeRSpecExample.new :pending, type:     :routing,
                                                    location: 'some_spec2.rb:1',
                                                    rfc:      [:sub__make_test_better]
  end

  describe '#header' do
    it 'contains the Spec name' do
      expect(instance.header).to include(spec.name)
    end

    it 'contains the Spec URL' do
      expect(instance.header).to include("[#{spec.specs_url}]")
    end
  end

  it 'renders the right string' do
    expected = File.read('spec/fixtures/expected_markdown.md')

    actual = instance.render

    expect(actual).to eq expected
  end
end
