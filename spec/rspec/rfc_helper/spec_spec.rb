# frozen_string_literal: true

require 'spec_helper'
require 'rspec/rfc_helper/spec'

RSpec.describe RSpec::RfcHelper::Spec do
  let(:instance) { described_class.new section: '1', text: 'it [[SHOULD]] pass', id: :the_id }

  describe '.new', rfc: :features__can_define_spec do
    it 'sets instances variables' do # rubocop:disable RSpec/ExampleLength
      instance = described_class.new section: '1', text: 'it [[SHOULD]] pass', id: :the_id
      aggregate_failures do
        expect(instance.section).to eq '1'
        expect(instance.text).to eq 'it [[SHOULD]] pass'
        expect(instance.id).to eq :the_id
        expect(instance.verb).to eq :should
      end
    end
  end

  describe '#status' do
    context 'with no example' do
      it 'returns :unknown' do
        expect(instance.status).to eq :unknown
      end
    end

    context 'with a failing example' do
      before do
        instance.add_example(FakeRSpecExample.new(:failed))
      end

      it 'returns :failing' do
        expect(instance.status).to eq :failing
      end
    end

    context 'with a passing example' do
      before do
        instance.add_example(FakeRSpecExample.new(:passed))
      end

      it 'returns :pass' do
        expect(instance.status).to eq :pass
      end
    end

    context 'with a pending example' do
      before do
        instance.add_example(FakeRSpecExample.new(:pending))
      end

      it 'returns :has_pending' do
        expect(instance.status).to eq :has_pending
      end
    end

    context 'with an example of every type' do
      before do
        instance.add_example(FakeRSpecExample.new(:failed))
        instance.add_example(FakeRSpecExample.new(:passed))
        instance.add_example(FakeRSpecExample.new(:pending))
      end

      it 'returns :failing' do
        expect(instance.status).to eq :failing
      end
    end

    context 'with all examples passing' do
      before do
        instance.add_example(FakeRSpecExample.new(:passed))
        instance.add_example(FakeRSpecExample.new(:passed))
      end

      it 'returns :pass' do
        expect(instance.status).to eq :pass
      end
    end

    context 'with passing examples and a pending one' do
      before do
        instance.add_example(FakeRSpecExample.new(:passed))
        instance.add_example(FakeRSpecExample.new(:pending))
      end

      it 'returns :has_pending' do
        expect(instance.status).to eq :has_pending
      end
    end
  end

  describe '#add_example' do
    it 'adds the entry to the list' do
      instance.add_example 'foo'

      expect(instance.examples.last).to eq 'foo'
    end
  end
end
