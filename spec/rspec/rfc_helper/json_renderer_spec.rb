# frozen_string_literal: true

require 'spec_helper'

require 'rspec/rfc_helper/json_renderer'

RSpec.describe RSpec::RfcHelper::JsonRenderer do
  let(:spec) { RSpec::RfcHelper::Specs.new_from_file 'spec/fixtures/rfc.yaml' }
  let(:instance) { described_class.new spec }

  before do
    spec.add_example FakeRSpecExample.new :passed, type:     :system,
                                                   location: 'some_spec.rb:20',
                                                   rfc:      [:first__be_good, :sub__work]
    spec.add_example FakeRSpecExample.new :failed, type:     :routing,
                                                   location: 'some_spec2.rb:49',
                                                   rfc:      [:sub__make_test_better]
    spec.add_example FakeRSpecExample.new :pending, type:     :routing,
                                                    location: 'some_spec2.rb:1',
                                                    rfc:      [:sub__make_test_better]
  end

  describe 'render' do
    it 'returns a valid JSON string' do
      output = instance.render

      aggregate_failures do
        expect(output).to be_a String
        expect { JSON.parse(output) }.not_to raise_error
      end
    end

    it 'returns the right string' do
      expected = File.read('spec/fixtures/expected_json.json').chomp

      expect(instance.render).to eq expected
    end
  end
end
