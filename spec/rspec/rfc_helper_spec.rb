# frozen_string_literal: true

RSpec.describe Rspec::RfcHelper do
  it 'has a version number', rfc: :features__have_version do
    expect(Rspec::RfcHelper::VERSION).not_to be_nil
  end
end
