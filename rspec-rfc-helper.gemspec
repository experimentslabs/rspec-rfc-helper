# frozen_string_literal: true

require_relative 'lib/rspec/rfc_helper/version'

Gem::Specification.new do |spec|
  spec.name = 'rspec-rfc-helper'
  spec.version = Rspec::RfcHelper::VERSION
  spec.authors = ['Manuel Tancoigne']
  spec.email = ['manu@experimentslabs.com']

  spec.summary = 'RSpec plugin to track RFC compliance'
  spec.description = 'Helps keeping track of specifications with more context than just RSpec examples'
  spec.homepage = 'https://gitlab.com/experimentslabs/rspec-rfc-helper'
  spec.required_ruby_version = '>= 3.1.2'

  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/experimentslabs/rspec-rfc-helper'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/experimentslabs/rspec-rfc-helper/-/blob/main/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor Gemfile])
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  spec.add_dependency 'rspec', '~> 3.0'
end
