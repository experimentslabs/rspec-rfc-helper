# Compliance status: RSpec RFC Helper

Source specification: [https://gitlab.com/experimentslabs/rspec-rfc-helper/-/blob/main/specification_example.md](https://gitlab.com/experimentslabs/rspec-rfc-helper/-/blob/main/specification_example.md)

| status | id | section | verb | text |
|-------:|---:|---------|------|------|
| <span style='color: green'>pass</span> | [`features__have_version`](#features__have_version) | 2.1 | must | The RFC Helper <mark>MUST</mark> have a version number |
| <span style='color: green'>pass</span> | [`features__can_define_spec`](#features__can_define_spec) | 2.1 | must | The RFC Helper <mark>MUST</mark> have methods to define specs that needs to be enforced and tracked. |
| <span style='color: gray'>unknown</span> | [`features__wrap_verbs`](#features__wrap_verbs) | 2.1 | must | To tackle this issue, the text used in a spec defined for the RFC Helper <mark>MUST</mark> wrap the right verb in the relevant  <br> paragraph into double square brackets, like, for example `<mark>VERB</mark>`. The same paragraph MAY be used in multiple specs, <br> with everytime a different verb wrapper. <br>  |
| <span style='color: gray'>unknown</span> | [`features__use_same_paragraph_twice`](#features__use_same_paragraph_twice) | 2.1 | may | To tackle this issue, the text used in a spec defined for the RFC Helper MUST wrap the right verb in the relevant  <br> paragraph into double square brackets, like, for example `<mark>VERB</mark>`. The same paragraph <mark>MAY</mark> be used in multiple specs, <br> with everytime a different verb wrapper. <br>  |
| <span style='color: green'>pass</span> | [`features__identify_specs_with_id`](#features__identify_specs_with_id) | 2.1 | must | Defined specs also need to be identifiable, so a unique ID <mark>MUST</mark> be assigned to them. |
| <span style='color: gray'>unknown</span> | [`features__no_verb_means_note`](#features__no_verb_means_note) | 2.1 | note | When a portion of text is interesting but has no imperative verb in it, it still can be added, and the RFC Helper will <br> give it a status of "note". <br>  |
| <span style='color: green'>pass</span> | [`reporting__rfc_tag`](#reporting__rfc_tag) | 2.2 | must | Once the RFC helper is configured in a RSpec suite, it <mark>MUST</mark> handle examples tagged with the `rfc` tag. |
| <span style='color: green'>pass</span> | [`reporting__rfc_with_single_id_or_list`](#reporting__rfc_with_single_id_or_list) | 2.2 | must | The RFC helper <mark>MUST</mark> handle a single ID, as well as an array of IDs. |
| <span style='color: green'>pass</span> | [`reporting__export_method`](#reporting__export_method) | 2.2 | must | The RFC Helper <mark>MUST</mark> have at least one method to export the generated report in a file. |
| <span style='color: gray'>unknown</span> | [`reporting__export_date`](#reporting__export_date) | 2.2 | must | The report <mark>MUST</mark> contain the generation date. |
| <span style='color: gray'>unknown</span> | [`usage__two_ways`](#usage__two_ways) | 3 | should | To have some flexibility, the helper <mark>SHOULD</mark> be useable in two ways: using the module `RSpec::RfcHelper`, or the classes. <br>  |
| <span style='color: gray'>unknown</span> | [`usage_module__start_method`](#usage_module__start_method) | 3.1 | must | The `RSpec::RfcHelper` <mark>MUST</mark> have a method to "start" the helper |
| <span style='color: gray'>unknown</span> | [`usage_module__handle_examples`](#usage_module__handle_examples) | 3.1 | must | It <mark>MUST</mark> have a method to add examples to specs once they are evaluated |
| <span style='color: gray'>unknown</span> | [`usage_module__export_method`](#usage_module__export_method) | 3.1 | must | it <mark>MUST</mark> have a method to save the report |
| <span style='color: gray'>unknown</span> |  | 3.2 | note | The RFC Helper SHOULD be used directly by instantiating a class. |
| <span style='color: gray'>unknown</span> |  | 3.2 | note | The instance MUST provide methods to add sections |
| <span style='color: gray'>unknown</span> |  | 3.2 | note | The instance MUST provide methods to define the specs |
| <span style='color: gray'>unknown</span> |  | 3.2 | note | The instance MUST provide methods to generate the report |
| <span style='color: gray'>unknown</span> |  | 3.2 | note | The instance MAY provide methods to load sections and specs from a file |

## <a id="features__have_version"></a><span style='color: green'>○</span> `MUST` features__have_version

**Specification:** 2.1 - Features

> The RFC Helper <mark>MUST</mark> have a version number

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: green'>passed</span> | `./spec/rspec/rfc_helper_spec.rb:4` |

## <a id="features__can_define_spec"></a><span style='color: green'>○</span> `MUST` features__can_define_spec

**Specification:** 2.1 - Features

> The RFC Helper <mark>MUST</mark> have methods to define specs that needs to be enforced and tracked.

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: green'>passed</span> | `./spec/rspec/rfc_helper/spec_spec.rb:10` |
| <span style='color: green'>passed</span> | `./spec/rspec/rfc_helper/specs_spec.rb:64` |

## <a id="features__wrap_verbs"></a><span style='color: gray'>○</span> `MUST` features__wrap_verbs

**Specification:** 2.1 - Features

> To tackle this issue, the text used in a spec defined for the RFC Helper <mark>MUST</mark> wrap the right verb in the relevant  <br> paragraph into double square brackets, like, for example `<mark>VERB</mark>`. The same paragraph MAY be used in multiple specs, <br> with everytime a different verb wrapper. <br> 

**No related examples**

## <a id="features__use_same_paragraph_twice"></a><span style='color: gray'>○</span> `MAY` features__use_same_paragraph_twice

**Specification:** 2.1 - Features

> To tackle this issue, the text used in a spec defined for the RFC Helper MUST wrap the right verb in the relevant  <br> paragraph into double square brackets, like, for example `<mark>VERB</mark>`. The same paragraph <mark>MAY</mark> be used in multiple specs, <br> with everytime a different verb wrapper. <br> 

**No related examples**

## <a id="features__identify_specs_with_id"></a><span style='color: green'>○</span> `MUST` features__identify_specs_with_id

**Specification:** 2.1 - Features

> Defined specs also need to be identifiable, so a unique ID <mark>MUST</mark> be assigned to them.

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: green'>passed</span> | `./spec/rspec/rfc_helper/specs_spec.rb:75` |

## <a id="features__no_verb_means_note"></a><span style='color: gray'>○</span> `NOTE` features__no_verb_means_note

**Specification:** 2.1 - Features

> When a portion of text is interesting but has no imperative verb in it, it still can be added, and the RFC Helper will <br> give it a status of "note". <br> 

**No related examples**

## <a id="reporting__rfc_tag"></a><span style='color: green'>○</span> `MUST` reporting__rfc_tag

**Specification:** 2.2 - Reporting

> Once the RFC helper is configured in a RSpec suite, it <mark>MUST</mark> handle examples tagged with the `rfc` tag.

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: green'>passed</span> | `./spec/rspec/rfc_helper/specs_spec.rb:91` |

## <a id="reporting__rfc_with_single_id_or_list"></a><span style='color: green'>○</span> `MUST` reporting__rfc_with_single_id_or_list

**Specification:** 2.2 - Reporting

> The RFC helper <mark>MUST</mark> handle a single ID, as well as an array of IDs.

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: green'>passed</span> | `./spec/rspec/rfc_helper/specs_spec.rb:91` |

## <a id="reporting__export_method"></a><span style='color: green'>○</span> `MUST` reporting__export_method

**Specification:** 2.2 - Reporting

> The RFC Helper <mark>MUST</mark> have at least one method to export the generated report in a file.

**Examples:**

| Status | Reference |
|:------:|-----------|
| <span style='color: green'>passed</span> | `./spec/rspec/rfc_helper/specs_spec.rb:103` |

## <a id="reporting__export_date"></a><span style='color: gray'>○</span> `MUST` reporting__export_date

**Specification:** 2.2 - Reporting

> The report <mark>MUST</mark> contain the generation date.

**No related examples**

## <a id="usage__two_ways"></a><span style='color: gray'>○</span> `SHOULD` usage__two_ways

**Specification:** 3 - Usage

> To have some flexibility, the helper <mark>SHOULD</mark> be useable in two ways: using the module `RSpec::RfcHelper`, or the classes. <br> 

**No related examples**

## <a id="usage_module__start_method"></a><span style='color: gray'>○</span> `MUST` usage_module__start_method

**Specification:** 3.1 - Using the module

> The `RSpec::RfcHelper` <mark>MUST</mark> have a method to "start" the helper

**No related examples**

## <a id="usage_module__handle_examples"></a><span style='color: gray'>○</span> `MUST` usage_module__handle_examples

**Specification:** 3.1 - Using the module

> It <mark>MUST</mark> have a method to add examples to specs once they are evaluated

**No related examples**

## <a id="usage_module__export_method"></a><span style='color: gray'>○</span> `MUST` usage_module__export_method

**Specification:** 3.1 - Using the module

> it <mark>MUST</mark> have a method to save the report

**No related examples**

## <span style='color: gray'>○</span> `NOTE` 

**Specification:** 3.2 - Using the classes

> The RFC Helper SHOULD be used directly by instantiating a class.

**No related examples**

## <span style='color: gray'>○</span> `NOTE` 

**Specification:** 3.2 - Using the classes

> The instance MUST provide methods to add sections

**No related examples**

## <span style='color: gray'>○</span> `NOTE` 

**Specification:** 3.2 - Using the classes

> The instance MUST provide methods to define the specs

**No related examples**

## <span style='color: gray'>○</span> `NOTE` 

**Specification:** 3.2 - Using the classes

> The instance MUST provide methods to generate the report

**No related examples**

## <span style='color: gray'>○</span> `NOTE` 

**Specification:** 3.2 - Using the classes

> The instance MAY provide methods to load sections and specs from a file

**No related examples**
