# RSpec RFC Helper Specification

Well... kind of, it's for the example.

This is the specification on how `rspec-rfc-helper` must behave and should be used in a test suite.

They somehow lack of substance, as I'm not a spec writer...

### 1 - Overview

RSpec RFC Helper (_RFC Helper_) is a RSpec module to help implement complicated specs: it is often hard to keep track of
the external specifications and their implementation; leaving comments in the code is not ideal to understand the state
of the overall implementation.

### 2 - Features

#### 2.1 Spec definition

The RFC Helper MUST have a version number

The RFC Helper MUST have methods to define specs that needs to be enforced and tracked.

Sometimes, in RFC and other formal documents, the same paragraph can have multiple imperative verbs as `MUST`, `SHOULD`,
..., making it complicated to break down and summarize.
To tackle this issue, the text used in a spec defined for the RFC Helper MUST wrap the right verb in the relevant
paragraph into double square brackets, like, for example `[[VERB]]`. The same paragraph MAY be used in multiple specs,
with everytime a different verb wrapper.

Defined specs also need to be identifiable, so a unique ID MUST be assigned to them.

When a portion of text is interesting but has no imperative verb in it, it still can be added, and the RFC Helper will
give it a status of "note".

Specifications are often separated in multiple sections. These sections SHOULD be used to group the specs together.

#### 2.2 Reporting

Once the RFC helper is configured in a RSpec suite, it MUST handle examples tagged with the `rfc` tag.

As a value, developers needs to specify one or many spec IDs targeted by the example. The RFC helper MUST handle a
single ID, as well as an array of IDs.

The RFC Helper MUST have at least one method to export the generated report in a file.

The report MUST contain the generation date.

### 3 - Usage

To have some flexibility, the helper SHOULD be useable in two ways : using the module `RSpec::RfcHelper`, or the classes.

#### 3.1 Using the module

The `RSpec::RfcHelper` MUST have a method to "start" the helper, reading a file with sections and specs. It MUST have a
method to add examples to specs once they are evaluated, and it MUST have a method to save the report.

Using the module is similar to using a singleton. It has its goods and bad sides.

#### 3.2 Using the classes

The RFC Helper SHOULD be used directly by instantiating a class.

The instance MUST provide methods to add sections

The instance MUST provide methods to define the specs

The instance MUST provide methods to generate the report

The instance MAY provide methods to load sections and specs from a file

Using the classes is a bit more complex but allows more flexibility.

### 3 - Final words

Yeah! that's some specification!
